<?php

// Turn off all error reporting
// error_reporting(0);

// Turn on all error reporting
error_reporting(1);


// Include Reverie
require_once('inc/foundationpress/functions.php'); 

// Include Theme Functions
require_once('inc/functions/theme-functions.php'); 

// Include Admin Functions
require_once('inc/functions/admin-functions.php'); 

// Include Custom Post Types
// require_once('inc/functions/custom-post-types.php'); 

// Include Custom Taxonomies
// require_once('inc/functions/custom-taxonomies.php'); 

// Include Custom Admin Page
// require_once('inc/functions/custom-admin-page.php'); 

// Include Theme Customizer
// require_once('inc/functions/customize-functions.php'); 

// Include Woocommerce Styles
require_once('inc/functions/woocommerce-functions.php'); 

?>