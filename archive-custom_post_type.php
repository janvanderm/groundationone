<?php get_header(); ?>

<div class="row">

	<div class="small-12 large-8 columns" id="content" role="main">

	<?php
	$wp_query = null;
	$wp_query = new WP_Query();
	$wp_query->query('showposts=3&post_type=custom_post_type'.'&paged='.$paged);
	if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

	<?php get_template_part( 'content', get_post_format() ); ?>

	<?php endwhile; ?>

	<?php if ( function_exists('reverie_pagination') ) { reverie_pagination(); } else if ( is_paged() ) { ?>
		<nav id="post-nav">
			<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'reverie' ) ); ?></div>
			<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'reverie' ) ); ?></div>
		</nav>
	<?php } ?>

	<?php $wp_query = null;
	$wp_query = $temp; ?>

	<?php else : ?>

	<?php get_template_part( 'content', 'none' ); ?>

	<?php endif; ?>

	</div>

	<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>