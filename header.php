<?php get_head(); ?>

<header class="contain-to-grid sticky">

	<div class="header-top">

		<div class="row">

			<div class="large-12 medium-12 columns">
				<div class="logo" onclick="window.location.href='<?php echo get_home_url(); ?>'">
					<?php echo get_field('header_text', 'options'); ?>
				</div>
			</div>
			
		</div>	

	</div>

	<?php get_template_part('inc/partials/top-bar'); ?>

</header>