function initWoocommerceCustom() {

	// Equal heights the product blocks on catalog pages 

	jQuery('ul.products li.product').matchHeight();

	// Execute only on woocommerce pages

	if (jQuery('body').hasClass('woocommerce-page')) {

		// Focus on first input field on checkout page for better user flow

		if (jQuery('body').hasClass('woocommerce-checkout')) {

			setTimeout(function() {
				jQuery('#billing_first_name').focus();
			}, 600);

		}

		// Price Filter Submit on mouseup

		jQuery('body').on("mouseup", ".ui-slider-handle", function(e) {
			e.preventDefault();
			console.log('test');
			jQuery(this).parent().parent().find('.button').click();

		});

	}

}

jQuery(function() {

	initWoocommerceCustom();

});