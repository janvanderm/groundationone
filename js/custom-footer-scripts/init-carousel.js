jQuery('.image-carousel').owlCarousel({
    items: 10,
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 2500,
    autoplayHoverPause: true,
    responsiveClass:true,
    responsive:{
        0: { items: 3 },
        600: { items: 6 },
        1000: { items: 9 }
    }
});