if (log==1) {console.log('foundation loaded')};

jQuery(document).foundation();

// Correct top-bar touch navigation dropdown click

if (Modernizr.touch) { 
    
	jQuery('li.menu-item.has-dropdown > a').attr({href: '#'});;

}

jQuery('.has-dropdown').on('mouseout', function () {
	jQuery(this).find('.dropdown').hide()
});
jQuery('.has-dropdown').on('mousemove', function () {
	jQuery(this).find('.dropdown').show()
});