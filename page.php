<?php get_header(); ?>

<?php // get_template_part( 'inc/partials/royalslider'); ?>

<?php // get_template_part( 'inc/partials/email-form'); ?>

<?php // get_template_part( 'inc/partials/social-icons'); ?>

<?php // get_template_part( 'inc/partials/readmore'); ?>

<?php // get_template_part( 'inc/partials/owl-carousel'); ?>

<section class="container" role="document">

	<div class="main-content">

	<?php get_template_part( 'inc/partials/content-blocks'); ?>	

	</div>

</section>

<?php get_footer(); ?>