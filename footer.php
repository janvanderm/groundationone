			<div class="full-width footer-widget">
				<div class="row">
					<?php do_action('foundationPress_before_footer'); ?>
					<?php dynamic_sidebar("footer-widgets"); ?>
					<?php do_action('foundationPress_after_footer'); ?>
				</div>
			</div>

			<footer class="full-width" role="contentinfo">
				<div class="row">
					<div class="large-12 columns">

					<?php
						wp_nav_menu( array(
							'theme_location' => 'footer',
							'menu_class' => 'inline',
							'depth' => 1
						) ); 
					?>

					</div>
				</div>
				<div class="row ">
					<div class="large-12 columns">
						<p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. Footer copyright text and such.</p>
					</div>
				</div>
			</footer>

			<a class="exit-off-canvas"></a>
			
			<?php do_action('foundationPress_layout_end'); ?>
		
		</div>

	</div>
	
<?php wp_footer(); ?>

<?php do_action('foundationPress_before_closing_body'); ?>

<!-- W3TC-include-css -->

</body>

</html>