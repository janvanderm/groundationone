<?php get_header(); ?>

<!-- Start the main container -->
<div class="container" role="document">
	<div class="row"> 
		<!-- Row for main content area -->
			
		<?php if ( !is_single() ) { ?>

			<?php get_sidebar(); ?>

			<div class="small-12 large-9 columns" role="main" id="content">

				<?php woocommerce_get_template( 'archive-product.php' ); ?>

			</div>
		

		<?php } else { ?>
	
		<div class="small-12 large-12 columns" role="main">
	
			<?php woocommerce_content(); ?>

		</div>

		<?php } ?>

	</div><!-- Row End -->
</div><!-- Container End -->
		
<?php get_footer(); ?>