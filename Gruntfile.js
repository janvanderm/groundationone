module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		// SASS
		'sass': { // Task
			frontend: { // Target
				options: { // Target options
					outputStyle: 'compressed'
					// outputStyle: 'expanded'
				},
				files: { // Dictionary of files
					'css/app.css': 'scss/app.scss',
					// 'destination': 'source'
				}
			},
			login: { // Target
				options: { // Target options
					outputStyle: 'compressed'
				},
				files: { // Dictionary of filesappp
					'css/login.css': 'scss/login.scss',
					// 'destination': 'source'
				}
			},
			editor: { // Target
				options: { // Target options
					outputStyle: 'compressed'
				},
				files: { // Dictionary of files
					'css/editor.css': 'scss/editor.scss',
					// 'destination': 'source'
				}
			}
		},

		// UGLIFY
		'uglify': {
			app_header_scripts: {
				options: {
					compress: true,
					// beautify: true,
					// preserveComments: true,
					sourceMap: true,
					// sourceMapIncludeSources: true

				},
				files: {
					'js/header.min.js': [

						// 'js/custom-header-scripts/*.js',

						]
				}
			},
			app_footer_scripts: {
				options: {
					compress: true,
					// beautify: true,
					// preserveComments: true,
					sourceMap: true,
					// sourceMapIncludeSources: true

				},
				files: {
					'js/footer.min.js': [
						
						'bower_components/foundationStickyFooter/stickyFooter.js',

						'js/custom-header-scripts/*.js',

						'bower_components/modernizr/modernizr.js',

						'bower_components/fastclick/lib/fastclick.js',					

						// 'bower_components/jquery/dist/jquery.js',
						
						'bower_components/foundation/js/foundation/foundation.js',
						// 'bower_components/foundation/js/foundation/foundation.abide.js',
						// 'bower_components/foundation/js/foundation/foundation.accordion.js',
						// 'bower_components/foundation/js/foundation/foundation.alert.js',
						// 'bower_components/foundation/js/foundation/foundation.clearing.js',
						// 'bower_components/foundation/js/foundation/foundation.dropdown.js',
						// 'bower_components/foundation/js/foundation/foundation.equalizer.js',
						// 'bower_components/foundation/js/foundation/foundation.interchange.js',
						// 'bower_components/foundation/js/foundation/foundation.joyride.js',
						// 'bower_components/foundation/js/foundation/foundation.magellan.js',
						// 'bower_components/foundation/js/foundation/foundation.offcanvas.js',
						// 'bower_components/foundation/js/foundation/foundation.orbit.js',
						// 'bower_components/foundation/js/foundation/foundation.reveal.js',
						// 'bower_components/foundation/js/foundation/foundation.slider.js',
						// 'bower_components/foundation/js/foundation/foundation.tab.js',
						// 'bower_components/foundation/js/foundation/foundation.tooltip.js',
						'bower_components/foundation/js/foundation/foundation.topbar.js',

						'bower_components/owl.carousel/dist/owl.carousel.js',

						'bower_components/iosweblinks/dist/webapplinks.js',

						'bower_components/royalslider/royalslider/jquery.royalslider.min.js',

						'bower_components/matchHeight/jquery.matchHeight.js',

						// 'js/vendor/jquery.magnific-popup.min.js',

						'js/custom-footer-scripts/*.js'

						]
				}
			},
			app_admin_scripts: {
				options: {
					compress: true,
					// beautify: true,
					// preserveComments: true,
					sourceMap: true,
					// sourceMapIncludeSources: true

				},
				files: {
					'js/admin.min.js': [

						'js/custom-admin-scripts/*.js'
						
						]
				}
			},
			app_admin_tinymce_scripts: {
				options: {
					beautify: false
				},
				files: {
					'js/admin-tinymce.min.js': [

						// 'js/vendor/typekit.tinymce.js',
						
						]
				}
			}

		},

		// CREATE DEVICE ICONS

		favicons: {
			options: {
				regular: true,
				apple: true,
				firefox: false,
				androidHomescreen: true,
				firefoxRound: false,
				appleTouchPadding: 0,
				sharp: 0.9,
				trueColor: true,
				precomposed: false,
				// appleTouchBackgroundColor: "red",
				coast: true,
				windowsTile: true,
				tileBlackWhite: false,
				tileColor: "auto",
				html: 'inc/partials/device-icons.php',
				HTMLPrefix: "<?php echo get_template_directory_uri(); ?>/assets/img/device-icons/"
			},
			icons: {
				src: 'assets/img/templates/icon-master.png',
				dest: 'assets/img/device-icons/'
			}
		},


		// PHONEGAP SPLASH

		phonegapsplash: {
		all_phones: {
			src: 'assets/img/templates/icon-master.png',
			dest: 'assets/img/device-splashscreens/'
			}
		},	

		// WATCH TASK
		'watch': {
			grunt: {
				files: ['Gruntfile.js'],
				tasks: ['uglify'],
				options: {
					livereload: true,
				}				
			},
			device_icons: {
				files: ['assets/img/templates/icon-master.png'],
				tasks: ['favicons'],
				options: {
					livereload: true,
				}				
			},
			js_all: {
				files: ['js/library/*.js','js/vendor/*.js'],
				tasks: ['uglify'],
				options: {
					livereload: true,
				}				
			},			
			js_admin: {
				files: ['js/custom-admin-scripts/*.js'],
				tasks: ['uglify:app_admin_tinymce_scripts','uglify:app_admin_tinymce_scripts'],
				options: {
					livereload: true,
				}				
			},
			js_header: {
				files: ['js/custom-header-scripts/*.js'],
				tasks: ['uglify:app_header_scripts'],
				options: {
					livereload: true,
				}				
			},
			js_footer: {
				files: ['js/custom-footer-scripts/*.js'],
				tasks: ['uglify:app_footer_scripts'],
				options: {
					livereload: true,
				}				
			},						
			css_app: {
				files: ['scss/app.scss'],
				tasks: ['sass:frontend'],
				options: {
					livereload: true,
				}				
			},
			css_all: {
				files: ['scss/editor.scss','scss/login.scss', 'scss/_*.scss','scss/partials/*.scss','scss/wordpress/*.scss','scss/tools/**/*.scss'],
				tasks: ['sass'],
				options: {
					livereload: true,
				}				
			},
			other: {
				files: ['*.php','*.html', 'inc/**/*.inc','inc/**/*.*','assets/**/*.*','woocommerce/**/*.*'],
				options: {
					livereload: true,
				}				
			},			
		},

	});


	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-compress');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-favicons');
	grunt.loadNpmTasks('grunt-phonegapsplash');

	grunt.registerTask('splash', ['phonegapsplash','rename']);
	grunt.registerTask('default', ['watch']);
	// grunt.registerTask('backup', ['compress:backup','ftp-deploy:backup']);
	// grunt.registerTask('build', ['uglify', 'sass', 'favicons', 'imagemin', 'compress', 'backup', 'watch']);

};