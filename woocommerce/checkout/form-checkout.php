<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

// do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() ); ?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( $get_checkout_url ); ?>" enctype="multipart/form-data">

		<div class="row" id="customer_details">

				<div class="small-12 large-7 -large-push-5 columns">

					<fieldset>

					<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

						<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

						<div class="row" id="customer_details">

							<div class="small-12 large-12 columns">


								<?php do_action( 'woocommerce_checkout_billing' ); ?>

								<?php do_action( 'woocommerce_checkout_shipping' ); ?>

							</div>

							<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

						</div>

					<?php endif; ?>

					</fieldset>	

				</div>

				<div class="small-12 large-5 columns -large-pull-7">

				<?php do_action( 'woocommerce_checkout_order_review' ); ?>

				</div>
			
		</div>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
