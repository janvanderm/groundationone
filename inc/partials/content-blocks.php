		<div class="row">

			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

			<?php if( have_rows( 'content_block') ): ?>
				
				<?php while ( have_rows( 'content_block') ) : the_row(); ?>

				<?php if( get_row_layout()=='anchor' ): ?>
					
					<div class="small-12 large-12 columns anchor">
						<a name="<?php echo urlencode(get_sub_field( 'titel')); ?>"></a>
					</div>

				<?php elseif( get_row_layout()=='horizontal_line' ): ?>
				
					<div class="small-12 large-12 columns ">
						<hr>
					</div>

				<?php elseif( get_row_layout()=='blank_line' ): ?>
				
					<div class="small-12 large-12 columns ">
						<br />
					</div>				        
									

				<?php elseif( get_row_layout()=='one_column' ): ?>
				
					<div class="small-12 large-12 columns ">
						<?php echo get_sub_field( 'column'); ?>
					</div>


				<?php elseif( get_row_layout()=='two_columns' ): ?>
				
					<div class="small-12 large-6 columns ">
						<?php echo get_sub_field( 'column_one'); ?>
						<br class="hide-for-large">
						<br class="hide-for-large">
						<hr class="hide-for-large">
						<br class="hide-for-large">				            
					</div>

					<div class="small-12 large-6 columns ">
						<?php echo get_sub_field( 'column_two'); ?>
						<br class="show-for-small">
					</div>
				

				<?php elseif( get_row_layout()=='three_columns' ): ?>
				
					<div class="small-12 large-4 columns ">
						<?php echo get_sub_field( 'column_one'); ?>
					</div>
					<div class="small-12 large-4 columns ">
						<?php echo get_sub_field( 'column_two'); ?>
					</div>
					<div class="small-12 large-4 columns ">
						<?php echo get_sub_field( 'column_three'); ?>
					</div>
				

				<?php elseif( get_row_layout()=='four_columns' ): ?>
				
					<div class="small-12 large-3 columns ">
						<?php echo get_sub_field( 'column_one'); ?>
						<br class="show-for-small">
					</div>
					<div class="small-12 large-3 columns ">
						<?php echo get_sub_field( 'column_two'); ?>
						<br class="show-for-small">
					</div>
					<div class="small-12 large-3 columns ">
						<?php echo get_sub_field( 'column_three'); ?>
						<br class="show-for-small">
					</div>
					<div class="small-12 large-3 columns ">
						<?php echo get_sub_field( 'column_four'); ?>
						<br class="show-for-small">
					</div>
				

				<?php elseif( get_row_layout()=='one_column_three_columns' ): ?>
				
					<div class="small-12 large-4 columns ">
						<?php echo get_sub_field( 'column_one'); ?>
					</div>
					<div class="small-12 large-8 columns ">
						<?php echo get_sub_field( 'column_two'); ?>
					</div>


				<?php elseif( get_row_layout()=='three_columns_one_column' ): ?>
				
					<div class="small-12 large-8 columns ">
						<?php echo get_sub_field( 'column_one'); ?>
					</div>
					<div class="small-12 large-4 columns ">
						<?php echo get_sub_field( 'column_two'); ?>
					</div>

				<?php elseif( get_row_layout()=='one_column_two_columns' ): ?>
				
					<div class="small-12 large-5 columns ">
						<?php echo get_sub_field( 'column_one'); ?>
					</div>
					<div class="small-12 large-7 columns ">
						<?php echo get_sub_field( 'column_two'); ?>
					</div>	

				<?php elseif( get_row_layout()=='slider' ): ?>

						<?php get_template_part( 'inc/partials/royalslider'); ?>

				<?php elseif( get_row_layout()=='intro-visual' ): ?>

					<header>
						<div class="intro-visual" style="background-image: url(' <?php $image = wp_get_attachment_image_src( get_sub_field('afbeelding') ,'large',0); echo $image[0]; ?> '); ">
							<div>
								<h1 class="entry-title heading-one"><?php echo get_sub_field('titel');; ?></h1>
							</div>
						</div>
						<br>
					</header>

				<?php elseif( get_row_layout()=='acties_3' ): ?>

					<div class="small-12 medium-4 large-4 columns action-block" onclick="window.location.href='<?php $link = get_sub_field('actie_1_link'); echo get_the_permalink($link[0]); ?>'">
						
						<div style="background-image: url('<?php $image_1 = wp_get_attachment_image_src( get_sub_field('actie_1_image') ,'medium',0); echo $image_1[0]; ?> ')"></div>
						<h3 class="heading-three"><?php echo get_sub_field('actie_1_titel'); ?></h3>

					</div>

					<div class="small-12 medium-4 large-4 columns action-block" onclick="window.location.href='<?php $link = get_sub_field('actie_2_link'); echo get_the_permalink($link[0]); ?>'">

						<div style="background-image: url('<?php $image_2 = wp_get_attachment_image_src( get_sub_field('actie_2_image') ,'medium',0); echo $image_2[0]; ?> ')"></div>
						<h3 class="heading-three"><?php echo get_sub_field('actie_2_titel'); ?></h3>
	
					</div>

					<div class="small-12 medium-4 large-4 columns action-block" onclick="window.location.href='<?php $link = get_sub_field('actie_3_link'); echo get_the_permalink($link[0]); ?>'">

						<div style="background-image: url('<?php $image_3 = wp_get_attachment_image_src( get_sub_field('actie_3_image') ,'medium',0); echo $image_3[0]; ?> ')"></div>
						<h3 class="heading-three"><?php echo get_sub_field('actie_3_titel'); ?></h3>
	
					</div>

				<?php elseif( get_row_layout()=='acties_4' ): ?>

					<div class="small-6 medium-3 large-3 columns action-block" onclick="window.location.href='<?php $link = get_sub_field('actie_1_link'); echo get_the_permalink($link[0]); ?>'">						

						<div style="background-image: url('<?php $image_1 = wp_get_attachment_image_src( get_sub_field('actie_1_image') ,'medium',0); echo $image_1[0]; ?> ')"></div>
						<h3 class="heading-three"><?php echo get_sub_field('actie_1_titel'); ?></h3>

					</div>

					<div class="small-6 medium-3 large-3 columns action-block" onclick="window.location.href='<?php $link = get_sub_field('actie_2_link'); echo get_the_permalink($link[0]); ?>'">

						<div style="background-image: url('<?php $image_2 = wp_get_attachment_image_src( get_sub_field('actie_2_image') ,'medium',0); echo $image_2[0]; ?> ')"></div>
						<h3 class="heading-three"><?php echo get_sub_field('actie_2_titel'); ?></h3>
	
					</div>

					<div class="small-6 medium-3 large-3 columns action-block" onclick="window.location.href='<?php $link = get_sub_field('actie_3_link'); echo get_the_permalink($link[0]); ?>'">

						<div style="background-image: url('<?php $image_3 = wp_get_attachment_image_src( get_sub_field('actie_3_image') ,'medium',0); echo $image_3[0]; ?> ')"></div>
						<h3 class="heading-three"><?php echo get_sub_field('actie_3_titel'); ?></h3>
	
					</div>

					<div class="small-6 medium-3 large-3 columns action-block" onclick="window.location.href='<?php $link = get_sub_field('actie_4_link'); echo get_the_permalink($link[0]); ?>'">

						<div style="background-image: url('<?php $image_4 = wp_get_attachment_image_src( get_sub_field('actie_4_image') ,'medium',0); echo $image_4[0]; ?> ')"></div>
						<h3 class="heading-three"><?php echo get_sub_field('actie_4_titel'); ?></h3>
	
					</div>					
				
				<?php elseif( get_row_layout()=='galleries' ): ?>

					<div class="small-12 large-12 columns ">

						<?php
						$args = array( 'posts_per_page' => 10, 'post_type' => 'gallery' );
						$lastposts = get_posts( $args );
						foreach ( $lastposts as $post ) :
						  setup_postdata( $post ); ?>

							<div class="small-12 medium-4 large-4 columns gallery-block" onclick="window.location.href='<?php $link = get_sub_field('actie_1_link'); echo get_the_permalink($link[0]); ?>'">
								
								<div style="background-image: url('<?php $image_1 = wp_get_attachment_image_src( get_field('thumbnail') ,'large',0); echo $image_1[0]; ?> ')"></div>
								<h3 class="heading-three"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

							</div>	

						<?php endforeach; 
						wp_reset_postdata(); ?>										

					</div>

				<?php elseif( get_row_layout()=='image_tickertape' ): ?>

					<div class="small-12 large-12 columns ">

							<?php 

							$images = get_sub_field('images');

							if( $images ): ?>
							    
							        <div class="image-carousel">
							            <?php foreach( $images as $image ): ?>
							                
							                    <div class="logo-container"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></div>
							                
							            <?php endforeach; ?>
							        </div>
							    
							<?php endif; ?>

					</div>

				<?php elseif( get_row_layout()=='content_sidebar' ): ?>

						<?php if (get_sub_field('content')): ?>

						<div class="small-12 large-8 columns ">

							<?php 
							foreach(get_sub_field('content') as $item) {

								if ($item['acf_fc_layout'] == 'custom_content') { ?>
									
									<?php echo $item['content']; ?>

								<?php }

								elseif ($item['acf_fc_layout'] == 'content_listing') { ?>
									
									<?php foreach ($item['items']as &$post_id) { ?>

									<?php $the_post = get_post($post_id); ?>

									<article>

										<h2 class="heading-two"><a href="<?php echo get_permalink( $post_id ); ?>"><?php echo $the_post->post_title; ?></a></h2>
										
											<?php if ( get_field('niveau', $post_id) != '' || get_field('aantal_lessen', $post_id) != '' ) { ?>

												<div class="post-meta">
													<p>

													<?php if ( get_field('niveau', $post_id) != '' ) { ?>
														<strong>Niveau:</strong> &nbsp;<span><?php echo get_field('niveau', $post_id); ?></span>
													<?php } ?>
													
													<?php if ( get_field('niveau', $post_id) != '' && get_field('aantal_lessen', $post_id) != '' ) { ?>
														&nbsp;|&nbsp;
													<?php } ?>

													<?php if ( get_field('aantal_lessen', $post_id) != '' ) { ?>
														<strong>Aantal lessen:</strong> &nbsp;<span><?php echo get_field('aantal_lessen', $post_id); ?></span>
													<?php } ?>

													</p>
												</div>		

											<?php } ?>

											<p><?php echo wp_trim_words( $the_post->post_content, 36 ); ?>&nbsp; <a href="<?php echo get_permalink( $post_id ); ?>">Lees meer </a></p>
											<br>

									</article>
									    
									<?php } ?>

								<?php }

								elseif ($item['acf_fc_layout'] == 'comments') { ?>
									<div class="panel">Comments</div>
								<?php }	

								elseif ($item['acf_fc_layout'] == 'agenda') { ?>

									<?php
									$args = array( 'posts_per_page' => 10, 'post_type' => 'agenda' );
									$lastposts = get_posts( $args );
									foreach ( $lastposts as $post ) :
									  setup_postdata( $post ); ?>
										<h2 class="heading-two"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

										<?php if ( get_field('datum', $post_id) != '' ) { ?>

											<div class="post-meta">
												<p>

												<?php if ( get_field('datum', $post_id) != '' ) { ?>
													<strong>Datum:</strong> &nbsp;<span><?php echo get_field('datum', $post_id); ?></span>
												<?php } ?>

												</p>
											</div>		

										<?php } ?>

										<p><?php echo wp_trim_words( $post->post_content, 36 ); ?>&nbsp; <a href="<?php echo get_permalink( $post_id ); ?>">Lees meer </a></p><br>
									<?php endforeach; 
									wp_reset_postdata(); ?>	

									<?php }	

								elseif ($item['acf_fc_layout'] == 'nieuws') { ?>
								
									<?php
									$args = array( 'posts_per_page' => 10 );
									$lastposts = get_posts( $args );
									foreach ( $lastposts as $post ) :
									  setup_postdata( $post ); ?>
										<h2 class="heading-two"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
										<p><?php echo wp_trim_words( $post->post_content, 36 ); ?>&nbsp; <a href="<?php echo get_permalink( $post_id ); ?>">Lees meer </a></p>
									<?php endforeach; 
									wp_reset_postdata(); ?>				

								<?php }															   							   

							} ?>							

						</div>

						<?php endif; ?>

						<?php get_template_part('sidebar'); ?>


			<?php endif; endwhile; ?> 
			
			<?php else : endif; ?>

			</article>

		</div>
