<?php 
$captcha_instance = new ReallySimpleCaptcha(); 
$captcha_instance->bg = array( 60, 60, 60 );
$captcha_instance->img_size = array( 120, 48 );
$captcha_instance->font_size = 24;
$captcha_instance->base = array( 10, 32 );
$captcha_instance->char_length = 6;
$word = $captcha_instance->generate_random_word();
$prefix = mt_rand();
?>

					<?php if (!isset($_GET['form']) || ($_GET['form']!=1)) { ?>

					<div class="row">

						<form id="order_form" method="post" action="<?php echo get_permalink();?>?form=1" data-abide>

					        <div class="small-12 large-12 columns ">
								<br>
							</div>						

					        <div class="small-12 large-6 columns ">
								
								<div class="name-field form-field row">
								    
								    <div class="small-12 large-4 columns">
								    	<label>Your name * </label>
								    </div>
								        
								    <div class="small-12 large-8 columns">
								    	<input type="text" name="Name" id="Name" required pattern="[a-zA-Z]+">
								    	<small class="error">Name is required and must be a string.</small>
								    </div>

								</div>

								<div class="company-field form-field row">
								    
								    <div class="small-12 large-4 columns">
								    	<label>Company * </label>
								    </div>
								        
								    <div class="small-12 large-8 columns">
								    	<input type="text" name="Company" id="Company" required pattern="[a-zA-Z]+">
								    	<small class="error">Company is required.</small>
								    </div>

								</div>

								<div class="address-field form-field row">
								    
								    <div class="small-12 large-4 columns">
								    	<label>Address * </label>
								    </div>
								        
								    <div class="small-12 large-8 columns">
								    	<input type="text" name="Address" id="Address" required>
								    	<small class="error">Name is required.</small>
								    </div>

								</div>

								<div class="postalcode-field form-field row">
								    
								    <div class="small-12 large-4 columns">
								    	<label>Postal Code * </label>
								    </div>
								        
								    <div class="small-12 large-8 columns">
								    	<input type="text" name="Postalcode" id="Postalcode" required>
								    	<small class="error">Postal code is required.</small>
								    </div>

								</div>

								<div class="city-field form-field row">

								    
								    <div class="small-12 large-4 columns">
								    	<label>City * </label>
								    </div>
								        
								    <div class="small-12 large-8 columns">
								    	<input type="text" name="City" id="City" required pattern="[a-zA-Z]+">
								    	<small class="error">CIty is required.</small>
								    </div>

								</div>

								<div class="country-field form-field row">
								    
								    <div class="small-12 large-4 columns">
								    	<label>Country * </label>
								    </div>
								        
								    <div class="small-12 large-8 columns">
								    	<input type="text" name="Country" id="Country" required pattern="[a-zA-Z]+">
								    	<small class="error">Country is required.</small>
								    </div>

								</div>

								<div class="email-field form-field row">
								    
								    <div class="small-12 large-4 columns">
								    	<label>E-mail * </label>
								    </div>
								        
								    <div class="small-12 large-8 columns">
								    	<input type="email" name="Email" id="Email" required>
								    	<small class="error">An email address is required.</small>
								    </div>

								</div>

								<div class="phone-field form-field row">
								    
								    <div class="small-12 large-4 columns">
								    	<label>Phone * </label>
								    </div>
								        
								    <div class="small-12 large-8 columns">
								    	<input type="text" name="Phone" id="Phone" required pattern="^[-+]?\d+$">
								    	<small class="error">Phonenumber is required and must be a number.</small>
								    </div>

								</div>

								<div class="phone-field form-field row">
								    
								    <div class="small-12 large-8 columns">
								    	<label>
								    		Security Check - Enter the sum of: &nbsp; 

								    		<strong>

									    	<?php 
											$a = rand(1,9);
											$b = rand(1,9);
											$c = rand(1,9);
											$d = $a + $b + $c;
											echo "" . $a . " + " . $b . " + " . $c; 
											?>

											</strong>

								    	</label>
								    	
								    </div>

								    <div class="small-12 large-4 columns">
								    	<input type="text" placeholder="<?php echo "" . $a . " + " . $b . " + " . $c;  ?>" name="Security" id="Security" required pattern="^<?php echo $d; ?>">
								    	<small class="error">Incorrect.</small>
								    </div>								    

								</div>								


								<div class="phone-field form-field row">
								    
								    <div class="small-12 large-4 columns">
								    	<label>Retype Captcha</label>
								    	
								    </div>

								    <div class="small-12 large-3 columns">
										<img src="/wptest/wp-content/plugins/really-simple-captcha/tmp/<?php echo $captcha_instance->generate_image( $prefix, $word ); ?>">								    	
									</div>

								    <div class="small-12 large-5 columns">	
								    	
								    	<input type="text" placeholder="" name="Security" id="Security" required pattern="<?php echo $word; ?>">
								    	
								    	<small class="error">Incorrect.</small>
								    </div>								    

								</div>								
	
							</div>								

							<div class="small-12 large-12 columns hide-for-large-up">

								<hr>


								<button type="submit" name="submit" value="Submit" class="expand">Submit</button>

							</div>

					        <div class="small-12 large-6 columns show-for-large-up">

					        	<div class="panel">

					        		<h2 class="heading-two">Items on quote request</h2>

								    <div class="message-field form-field row">

								    	<div class="small-12 large-12 columns ">

										    <label>Leave additional comments here</label>

										    <input type="text" id="Order" name="Order" style="display: none;">

										    <textarea placeholder="Message..." name="Message" id="Message"></textarea>

										    <small class="error">a message is required.</small>

										    <button type="submit" name="submit" value="Submit">Submit</button>

										</div>

							    	</div>

					        	</div>
								
							</div>

						</form>

					</div>


				<?php

				} elseif ($_GET['form']==1) { 

					// Form input to variables
					
					$Name = Trim(stripslashes($_POST['Name']));
					$Company = Trim(stripslashes($_POST['Company']));
					$Address = Trim(stripslashes($_POST['Address']));
					$Postalcode = Trim(stripslashes($_POST['Postalcode']));
					$City = Trim(stripslashes($_POST['City']));
					$Country = Trim(stripslashes($_POST['Country']));
					$Email = Trim(stripslashes($_POST['Email']));
					$Phone = Trim(stripslashes($_POST['Phone']));
					$Message = Trim(stripslashes($_POST['Message']));
					$Order = Trim(stripslashes($_POST['Order']));					

					// Header for e-mails

					$Headers = "From: noreply@cablesafe.com\r\n";
					$Headers .= "Reply-To: noreply@cablesafe.com\r\n";
					$Headers .= "MIME-Version: 1.0\r\n";
					$Headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

					// Message e-mail settings

					$MessageEmailFrom = "CableSafe";
					// $MessageEmailTo = get_sub_field( "e-mail_address");
					$MessageEmailTo = "jan@vandermeijde.net";
					$MessageSubject = "New quote request on ". get_bloginfo('name' )  . " - Notice";	

					// Confirmation e-mail settings

					// $ConfirmationEmailFrom = get_sub_field( "e-mail_address");
					$ConfirmationEmailFrom = "jan@vandermeijde.net";
					$ConfirmationEmailTo = $Email;
					$ConfirmationSubject = "Your quote request on ". get_bloginfo('name' )  . " - Confirmation";	

					// validation
					$validationOK=true;
					if (!$validationOK) {
					  print "error";
					  exit;
					}

					// Message email body text

					$MessageBody = "<img width='180' src='" . get_template_directory_uri() . "/img/email/email-logo.png'><br><br>";
					$MessageBody .= "<strong>New quote request</strong><br/>";
					$MessageBody .= "<br/>";
					$MessageBody .= "<p style='line-height: 22px;'>";					

					$MessageBody .= "Name: " . $Name . "<br/>";
					$MessageBody .= "Company: " . $Company . "<br/>";
					$MessageBody .= "Address: " . $Address . "<br/>";
					$MessageBody .= "Postalcode: " . $Postalcode . "<br/>";
					$MessageBody .= "City: " . $City . "<br/>";
					$MessageBody .= "Country: " . $Country . "<br/>";
					$MessageBody .= "Email: " . $Email . "<br/>";
					$MessageBody .= "Phone: " . $Phone . "<br/>";
					$MessageBody .= "<br/>";	
					$MessageBody .= "Request:";				
					$MessageBody .= "<br/>";
					$MessageBody .= "<br/>";					
					$MessageBody .= "<p/>";
					$MessageBody .= "<br/>";
					$MessageBody .= "Message:<br/>";
					$MessageBody .= "<p style='line-height: 22px;'>" . $Message . "</p><br/>";
					$MessageBody .= "<br/>";
					$MessageBody .= "Regards,<br/>";
					$MessageBody .= "<br/>";
					$MessageBody .= get_bloginfo('name' );
					$MessageBody .= "<br/>";


					// Confirmation email body text

					$ConfirmationBody = "<img width='180' src='" . get_template_directory_uri() . "/img/email/email-logo.png'><br><br>";
					$ConfirmationBody .= "<strong>Your request has been sent</strong><br/>";
					$ConfirmationBody .= "<br/>";
					$ConfirmationBody .= "<p style='line-height: 22px;'>";		
					
					$ConfirmationBody .= "Name: " . $Name . "<br/>";
					$ConfirmationBody .= "Company: " . $Company . "<br/>";
					$ConfirmationBody .= "Address: " . $Address . "<br/>";
					$ConfirmationBody .= "Postalcode: " . $Postalcode . "<br/>";
					$ConfirmationBody .= "City: " . $City . "<br/>";
					$ConfirmationBody .= "Country: " . $Country . "<br/>";
					$ConfirmationBody .= "Email: " . $Email . "<br/>";
					$ConfirmationBody .= "Phone: " . $Phone . "<br/>";
					$ConfirmationBody .= "<br/>";	
					$ConfirmationBody .= "Request:";				
					$ConfirmationBody .= "<br/>";
					$ConfirmationBody .= "<br/>";					
					$ConfirmationBody .= "<p/>";
					$ConfirmationBody .= "<br/>";
					$ConfirmationBody .= "Message:<br/>";
					$ConfirmationBody .= "<p style='line-height: 22px;'>" . $Message . "</p><br/>";
					$ConfirmationBody .= "<br/>";
					$ConfirmationBody .= "Regards,<br/>";
					$ConfirmationBody .= "<br/>";
					$ConfirmationBody .= get_bloginfo('name' );
					$ConfirmationBody .= "<br/>";	
 
					// Send email 
					$success = mail($MessageEmailTo, $MessageSubject, $MessageBody, $Headers); mail($ConfirmationEmailTo, $ConfirmationSubject, $ConfirmationBody, $Headers);

					// Show results
					if ($success){
					  echo '<div class="large-12 small-12 columns">';
					  echo '<div data-alert class="alert-box success radius">' . get_sub_field( "success_message") . '<a href="#" class="close">&times;</a></div>';
					  echo '<p>' . get_sub_field( "success_page_content") . '</p>';
					  echo '</div>';
					  
					}
					else{
					  echo '<div class="large-12 small-12 columns">';
					  echo '<div data-alert class="alert-box success radius">' . get_sub_field( "error_message") . '<a href="#" class="close">&times;</a></div>';
					  echo '<p>' . get_sub_field( "error_page_content") . '</p>';
					  echo '</div>';
					}	

				} ?>