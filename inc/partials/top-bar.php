<nav class="top-bar" data-topbar>

	<ul class="title-area">
		<li class="name">

				<?php if ( is_front_page() ) { ?>
				<h1 class="heading-five"><?php echo get_field('header_text', 'options'); ?></h1>
				<?php } else { ?>
				<h2 class="heading-five"><?php echo get_field('header_text', 'options'); ?></h2>
				<?php } ?>

		</li>
		
		<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
		<li class="toggle-topbar menu-icon">
			<a href="#">
				<span>Menu</span>
			</a>
		</li>

	</ul>

	<section class="top-bar-section">

	<?php
		wp_nav_menu( array(
			'theme_location' => 'top-bar-l',
			'container' => false,
			'depth' => 1
		) );
	?>

	</section>
	
</nav>
