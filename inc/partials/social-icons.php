<ul class="social social-small inline-list">
    <li class="text">Check us out on social media:</li>
    <li class="facebook" onclick="javascript:window.open('<?php // the_field('facebook', 'options'); ?>');"></li>
    <li class="twitter" onclick="javascript:window.open('<?php // the_field('twitter', 'options'); ?>');"></li>
    <li class="youtube" onclick="javascript:window.open('<?php // the_field('youtube', 'options'); ?>');"></li>
    <li class="linkedin" onclick="javascript:window.open('<?php // the_field('linkedin', 'options'); ?>');"></li>
    <li class="vimeo" onclick="javascript:window.open('<?php // the_field('vimeo', 'options'); ?>');"></li>
    <li class="google" onclick="javascript:window.open('<?php // the_field('google', 'options'); ?>');"></li>
    <li class="rss" onclick="javascript:window.open('<?php // the_field('rss', 'options'); ?>');"></li>
    <li class="email" onclick="javascript:window.open('<?php // the_field('email', 'options'); ?>');"></li>
</ul>