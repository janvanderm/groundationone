	<meta name="mobile-web-app-capable" value="yes">
	<meta name="msapplication-square70x70logo" content="<?php echo get_template_directory_uri(); ?>/assets/img/device-icons/windows-tile-70x70.png">
	<meta name="msapplication-square150x150logo" content="<?php echo get_template_directory_uri(); ?>/assets/img/device-icons/windows-tile-150x150.png">
	<meta name="msapplication-square310x310logo" content="<?php echo get_template_directory_uri(); ?>/assets/img/device-icons/windows-tile-310x310.png">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/img/device-icons/windows-tile-144x144.png">
	<meta name="msapplication-TileColor" content="#000000">
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/assets/img/device-icons/apple-touch-icon-152x152-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/assets/img/device-icons/apple-touch-icon-120x120-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/assets/img/device-icons/apple-touch-icon-76x76-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/assets/img/device-icons/apple-touch-icon-60x60-precomposed.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/img/device-icons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/img/device-icons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/img/device-icons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/img/device-icons/apple-touch-icon.png">
	<link rel="icon" sizes="228x228" href="<?php echo get_template_directory_uri(); ?>/assets/img/device-icons/coast-icon-228x228.png">
	<meta name="mobile-web-app-capable" value="yes">
	<link rel="icon" sizes="196x196" href="<?php echo get_template_directory_uri(); ?>/assets/img/device-icons/homescreen-196x196.png">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/device-icons/favicon.ico">
	<link rel="icon" type="image/png" sizes="64x64" href="<?php echo get_template_directory_uri(); ?>/assets/img/device-icons/favicon.png">
