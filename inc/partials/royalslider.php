<?php if ( get_sub_field('full_width')==1 ) { ?></div><?php } ?>

<div class="slider-block royalSlider rsUni <?php if ( get_sub_field('full_width')==1 ) { ?>full-width<?php } ?>">

    <?php 

    if(get_field( 'slides')) { while(the_repeater_field( 'slides')) { ?>

    <img src="<?php $image = wp_get_attachment_image_src(  get_sub_field('slide')  , 'large' ); echo $image[0]; ?>">

    <?php } } else { ?>

    <img src="http://placehold.it/1000x350">
    <img src="http://placehold.it/1000x350">
    <img src="http://placehold.it/1000x350">

    <?php } ?>

</div>

<?php if ( get_sub_field('full_width')==1 ) { ?><div class="row"><?php } ?>