<a class="test-popup-link" href="http://placehold.it/1000x350">Open popup</a> | <a class="test-popup-link" href="http://placehold.it/1000x350">Open popup</a>

<script>

jQuery(function() {
	jQuery('.test-popup-link').magnificPopup({ 
	  type: 'image'
		// other options
	});	
});

</script>

<br /><br />

<div class="parent-container">
  <a class="test-popup-gallery" href="http://placehold.it/1000x350">Open popup 1</a>
  <a class="test-popup-gallery" href="http://placehold.it/1000x350">Open popup 2</a>
  <a class="test-popup-gallery" href="http://placehold.it/1000x350">Open popup 3</a>
</div>

<script>
jQuery(function() {
	jQuery('.test-popup-gallery').magnificPopup({
	  type: 'image',
	  gallery:{
	    enabled:true
	  }
	});	
});

</script>

