<?php
/*********************
Enqueue the proper CSS
if you use Sass.
*********************/
function FoundationPress_sass_style()
{
	// Register the main style
	wp_register_style( 'FoundationPress-stylesheet', get_template_directory_uri() . '/css/app.css', array(), '', 'all' );
	wp_enqueue_style( 'FoundationPress-stylesheet' );
}
add_action( 'wp_footer', 'FoundationPress_sass_style' );

?>