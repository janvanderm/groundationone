<?php

// Custom post-types

add_action('init', 'register_custom_post_type');

function register_custom_post_type() {

register_post_type('custom_post_type', array(
  'label' => 'Post Type',
  'description' => '',
  'public' => true,
  'show_ui' => true,
  'show_in_menu' => true,
  'capability_type' => 'post',
  'map_meta_cap' => true,
  'hierarchical' => false,
  'rewrite' => array('slug' => 'custom-post-type', 'with_front' => true),
  'query_var' => true,
  'has_archive' => true,
  'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes','post-formats'),
  'labels' => array (
    'name' => 'Post Type',
    'singular_name' => 'Post Type',
    'menu_name' => 'Post Type',
    'add_new' => 'Add New',
    'add_new_item' => 'Add New ',
    'edit' => 'Edit',
    'edit_item' => 'Edit ',
    'new_item' => 'New ',
    'view' => 'View ',
    'view_item' => 'View ',
    'search_items' => 'Search s',
    'not_found' => 'None Found',
    'not_found_in_trash' => 'Nonone Found in Trash',
    'parent' => 'Parent', )
  ) 
); 

} ?>