<?php

// Head Function

function get_head() {
	get_template_part( 'inc/partials/head' );
	// include (TEMPLATEPATH . '/inc/partials/head.php');
} 


// Shortcode Example

function button_shortcode( $atts, $content = null ) {
	 extract( shortcode_atts( array(
			'link' => 'link'
			), $atts ) );
	 return '<a href="' . esc_attr($link) . '" class="button normal radius">' . $content . '</a>';
}
add_shortcode('button', 'button_shortcode');


// Add External Font stylesheets

// function add_external_fonts_stylesheet()
// {
// 	// Register the main style
// 	wp_register_style( 'fonts-com-stylesheet', 'http://fast.fonts.net/cssapi/55cb1f1f-d8f2-4fb5-8c39-b596ce482ef4.css', array(), '', 'all' );
// 	wp_enqueue_style( 'fonts-com-stylesheet' );
// }
// add_action( 'wp_enqueue_scripts', 'add_external_fonts_stylesheet' );

// Add Category To Body Class

function pn_body_class_add_categories( $classes ) {
	// Only proceed if we're on a single post page
	if ( !is_single() )
		return $classes;
	// Get the categories that are assigned to this post
	$post_categories = get_the_category();
	// Loop over each category in the $categories array
	foreach( $post_categories as $current_category ) {
		// Add the current category's slug to the $body_classes array
		$classes[] = 'category-' . $current_category->slug;

	}
	// Finally, return the $body_classes array
	return $classes;
}
add_filter( 'body_class', 'pn_body_class_add_categories' );



// Add Editor Styles

function add_custom_editor_styles() {
		add_editor_style( get_stylesheet_directory_uri(). '/css/editor.css' );
}
add_action( 'init', 'add_custom_editor_styles' );



// Add Editor Styles Dropdown Button

add_filter( 'mce_buttons_2', 'fb_mce_editor_buttons' );
function fb_mce_editor_buttons( $buttons ) {

		array_unshift( $buttons, 'styleselect' );
		return $buttons;
}

add_filter( 'tiny_mce_before_init', 'fb_mce_before_init' );

function fb_mce_before_init( $settings ) {

		$style_formats = array(
				array(
						'title' => 'Headers',
								'items' => array(
									array(  
										'title' => 'Heading One',  
										'block' => 'span',
										'classes' => 'heading-one',
										'wrapper' => true,
										
									),  
									array(  
										'title' => 'Heading Two',  
										'block' => 'span',
										'classes' => 'heading-two',
										'wrapper' => true,
										
									), 
									array(  
										'title' => 'Heading Three',  
										'block' => 'span',
										'classes' => 'heading-three',
										'wrapper' => true,
										
									),  
									array(  
										'title' => 'Heading Four',  
										'block' => 'span',
										'classes' => 'heading-four',
										'wrapper' => true,
										
									),	
									array(  
										'title' => 'Heading Five',  
										'block' => 'span',
										'classes' => 'heading-five',
										'wrapper' => true,
										
									),																		 									
						)
				),
				array(
						'title' => 'Download Link',
						'selector' => 'a',
						'classes' => 'download'
				)
		);

		$settings['style_formats'] = json_encode( $style_formats );

		return $settings;

}


// Remove Plugin Styles

// add_action( 'wp_print_styles', 'deregister_plugin_styles', 100 );
// function deregister_plugin_styles() {
// //    wp_deregister_style( 'PluginCss' );
// }
// add_action( 'wp_print_styles', 'deregister_search_styles_1', 100 );
// function deregister_search_styles_1() {
//   wp_deregister_style( 'search-filter-plugin-styles' );
// }
// add_action( 'wp_print_styles', 'deregister_search_styles_2', 100 );
// function deregister_search_styles_2() {
//   wp_deregister_style( 'search-filter-chosen-styles' );
// }
// add_action( 'wp_print_styles', 'deregister_formidable', 100 );
// function deregister_formidable() {
//   wp_deregister_style( 'formidable' );
// }
// add_action( 'wp_print_styles', 'deregister_ogone_cw', 100 );
// function deregister_ogone_cw() {
//   wp_deregister_style( 'jigoshop_ogonecw_frontend_styles' );
// }
// add_action( 'wp_print_styles', 'deregister_jigoshop_required', 100 );
// function deregister_jigoshop_required() {
//   wp_deregister_style( 'jigoshop_required' );
// }


// Add ACF Options Pages

// General options page with subpages

if( function_exists('acf_add_options_page') ) {
 
	$page = acf_add_options_page(array(
		'page_title'  => 'Custom Settings',
		'menu_title'  => 'Custom Settings',
		'menu_slug'   => 'custom-settings',
		'capability'  => 'edit_posts',
		'redirect'  => false,
		'position' => false,
		'icon_url' => false 
	));

	acf_add_options_sub_page(array(
			'title' => 'Header',
			'slug' => 'header',
			'parent' => 'custom-settings',
			'capability' => 'manage_options'
	)); 

	acf_add_options_sub_page(array(
			'title' => 'Footer',
			'slug' => 'footer',
			'parent' => 'custom-settings',
			'capability' => 'manage_options'
	)); 
	
 
}

// if( function_exists('acf_add_options_sub_page') )
// {
// 		acf_add_options_sub_page(array(
// 				'title' => 'Custom Settings',
// 				'parent' => 'options-general.php',
// 				'capability' => 'manage_options'
// 		));
// }



// Get the content by ID

function get_the_content_by_id($post_id) {
	$page_data = get_page($post_id);
	if ($page_data) {
		return $page_data->post_content;
	}
	else return false;
}


// Get the slug function

function get_the_slug( $id=null ){
	if( empty($id) ):
		global $post;
		if( empty($post) )
			return ''; // No global $post var available.
		$id = $post->ID;
	endif;
	$slug = basename( get_permalink($id) );
	return $slug;
}


// Strip Tags and Truncate 

function trunc_content($full_content,$length) {
		$full_content_stripped = strip_tags($full_content);
		if (strlen($full_content_stripped) >= $length) {
		return "" . substr($full_content_stripped, 0, strpos($full_content_stripped, " ", $length)) . "...";
		} else { return $full_content_stripped; }
}


// Page Slug Body Class

function add_slug_body_class( $classes ) {
global $post;
if ( isset( $post ) ) {
$classes[] = $post->post_type . '-' . $post->post_name;
}
return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );


// Disable All Comments

// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support() {
	$post_types = get_post_types();
	foreach ($post_types as $post_type) {
		if(post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
}
add_action('admin_init', 'df_disable_comments_post_types_support');


// Close comments on the front-end
function df_disable_comments_status() {
	return false;
}
add_filter('comments_open', 'df_disable_comments_status', 20, 2);
add_filter('pings_open', 'df_disable_comments_status', 20, 2);


// Hide existing comments
function df_disable_comments_hide_existing_comments($comments) {
	$comments = array();
	return $comments;
}
add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);


// Remove comments page in menu
function df_disable_comments_admin_menu() {
	remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'df_disable_comments_admin_menu');


// Redirect any user trying to access comments page
function df_disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
}
add_action('admin_init', 'df_disable_comments_admin_menu_redirect');


// Remove comments metabox from dashboard
function df_disable_comments_dashboard() {
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', 'df_disable_comments_dashboard');


// Remove comments links from admin bar
function df_disable_comments_admin_bar() {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	}
}
add_action('init', 'df_disable_comments_admin_bar');


// Move Native jQuery to footer

function md_footer_enqueue_scripts() {
    remove_action('wp_head', 'wp_print_scripts');
    remove_action('wp_head', 'wp_print_head_scripts', 9);
    remove_action('wp_head', 'wp_enqueue_scripts', 1);
}
add_action('wp_enqueue_scripts', 'md_footer_enqueue_scripts');


// Simple RSS parser
function getFeed($feed_url,$number) {   
		//$content = file_get_contents($feed_url);
		WP_Filesystem();
		global $wp_filesystem;
		$content = $wp_filesystem->get_contents($feed_url);
		$x = new SimpleXmlElement($content);
		$i = 0;
		foreach($x->channel->item as $entry) {
				$i++; if ($i <= $number) { echo "<h3 class='heading-three'><a href='$entry->link' target='_blank' title='$entry->title'>" . $entry->title . "</a></h3>";var_dump($entry); echo "<br/><br/>"; }
		}
}


// Add SVG media type upload
function cc_mime_types( $mimes ){
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );


// Check if user is logged in Shortcode

function check_user ($params, $content = null){
	//check tha the user is logged in
	if ( is_user_logged_in() ){
 
		//user is logged in so show the content
		return $content;
 
	}
 
	else{
 
		//user is not logged in so hide the content
		return;
 
	}
 
}
 
//add a shortcode which calls the above function
add_shortcode('loggedin', 'check_user' );
?>