<?php 

// Add Woocommerce theme support

add_theme_support( 'woocommerce' );

// Disable Woocommerce Styles

add_filter( 'woocommerce_enqueue_styles', '__return_false' );

// Change checkout button text

// add_filter( 'woocommerce_order_button_text', create_function( '', 'return "Proceed to payment";' ) );

// Add custom checkou field 

/**
 * Add the field to the checkout
 */

// add_action( 'woocommerce_after_checkout_billing_form', 'my_custom_checkout_field' );
 
// function my_custom_checkout_field( $checkout ) {
 
//     // echo '<div id="my_custom_checkout_field"><h2 class="heading-two">' . __('My Field') . '</h2>';
 
//     woocommerce_form_field( 'my_field_name', array(
//         'type'          => 'text',
//         'class'         => array('small-12 large-6 columns'),
//         'label'         => __('Custom field'),
//         'placeholder'   => __('Custom field value'),
//         ), $checkout->get_value( 'my_field_name' ));
 
//     // echo '</div>';
 
// }

/**
 * Process the checkout / validate
 */
// add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');
 
// function my_custom_checkout_field_process() {
//     // Check if set, if its not set add an error.
//     if ( ! $_POST['my_field_name'] )
//         wc_add_notice( __( 'Please enter something into this new shiny field.' ), 'error' );
// }

/**
 * Update the order meta with field value
 */

add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );
 
function my_custom_checkout_field_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['my_field_name'] ) ) {
        update_post_meta( $order_id, 'My Field', sanitize_text_field( $_POST['my_field_name'] ) );
    }
}

/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );
 
function my_custom_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('My Field').':</strong> ' . get_post_meta( $order->id, 'My Field', true ) . '</p>';
}

/**
 * Disable Reviews
 */

add_filter( 'woocommerce_product_tabs', 'sb_woo_remove_reviews_tab', 98);
function sb_woo_remove_reviews_tab($tabs) {

 unset($tabs['reviews']);

 return $tabs;
}

/**
 * Optimize WooCommerce Scripts
 * Remove WooCommerce Generator tag, styles, and scripts from non WooCommerce pages.
 */
// add_action( 'wp_enqueue_scripts', 'child_manage_woocommerce_styles', 99 );
 
// function child_manage_woocommerce_styles() {
//     //remove generator meta tag
//     remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );
 
//     //first check that woo exists to prevent fatal errors
//     if ( function_exists( 'is_woocommerce' ) ) {
//         //dequeue scripts and styles
//         if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
//             wp_dequeue_style( 'woocommerce_frontend_styles' );
//             wp_dequeue_style( 'woocommerce_fancybox_styles' );
//             wp_dequeue_style( 'woocommerce_chosen_styles' );
//             wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
//             wp_dequeue_script( 'wc_price_slider' );
//             wp_dequeue_script( 'wc-single-product' );
//             wp_dequeue_script( 'wc-add-to-cart' );
//             wp_dequeue_script( 'wc-cart-fragments' );
//             wp_dequeue_script( 'wc-checkout' );
//             wp_dequeue_script( 'wc-add-to-cart-variation' );
//             wp_dequeue_script( 'wc-single-product' );
//             wp_dequeue_script( 'wc-cart' );
//             wp_dequeue_script( 'wc-chosen' );
//             wp_dequeue_script( 'woocommerce' );
//             wp_dequeue_script( 'prettyPhoto' );
//             wp_dequeue_script( 'prettyPhoto-init' );
//             wp_dequeue_script( 'jquery-blockui' );
//             wp_dequeue_script( 'jquery-placeholder' );
//             wp_dequeue_script( 'fancybox' );
//             wp_dequeue_script( 'jqueryui' );
//         }
//     }
 
// }

// Remove checkout field

// add_filter('woocommerce_checkout_fields','custom_override_checkout_fields');

// function custom_override_checkout_fields( $fields ) {
//      unset($fields['order']['order_comments']);

//      return $fields;
// }


// Edit Existing Checkout fields

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     
     $fields['billing']['billing_country']['class'] = array('small-12','large-6','medium-6','columns');
     $fields['billing']['billing_first_name']['class'] = array('small-12','large-6','medium-6','columns');
     $fields['billing']['billing_last_name']['class'] = array('small-12','large-6','medium-6','columns');
     $fields['billing']['billing_company']['class'] = array('small-12','large-6','medium-6','columns');
     $fields['billing']['billing_address_1']['class'] = array('small-12','large-6','medium-6','columns');
     $fields['billing']['billing_address_2']['class'] = array('small-12','large-3','medium-3','columns');
     $fields['billing']['billing_address_2']['label'] = 'Nr. + Toev.';
     $fields['billing']['billing_postcode']['class'] = array('small-12','large-3','medium-3','columns');
     $fields['billing']['billing_city']['class'] = array('small-12','large-6','medium-6','columns');
     $fields['billing']['billing_email']['class'] = array('small-12','large-6','medium-6','columns');
     $fields['billing']['billing_phone']['class'] = array('small-12','large-6','medium-6','columns');

     $fields['shipping']['shipping_country']['class'] = array('small-12','large-6','medium-6','columns');
     $fields['shipping']['shipping_first_name']['class'] = array('small-12','large-6','medium-6','columns');
     $fields['shipping']['shipping_last_name']['class'] = array('small-12','large-6','medium-6','columns');
     $fields['shipping']['shipping_company']['class'] = array('small-12','large-6','medium-6','columns');
     $fields['shipping']['shipping_address_1']['class'] = array('small-12','large-6','medium-6','columns');
     $fields['shipping']['shipping_address_2']['class'] = array('small-12','large-3','medium-3','columns');
     $fields['shipping']['shipping_address_2']['label'] = 'Nr. + Toev.';
     $fields['shipping']['shipping_postcode']['class'] = array('small-12','large-3','medium-3','columns');
     $fields['shipping']['shipping_city']['class'] = array('small-12','large-6','medium-6','columns');
     
     $fields['shipping']['test']['label'] = 'Nr. + Toev.';
     $fields['order']['order_comments']['class'] = array('small-12','large-12','columns');



     return $fields;
}

// re order fields

add_filter("woocommerce_checkout_fields", "order_billing_fields");

function order_billing_fields($fields) {

    $order = array(
        "billing_first_name", 
        "billing_last_name", 
        "billing_address_1", 
        "billing_address_2", 
        "billing_postcode", 
        "billing_city", 
        "billing_country", 
        "billing_email", 
        "billing_phone",

    );
    foreach($order as $field)
    {
        $ordered_fields[$field] = $fields["billing"][$field];
    }

    $fields["billing"] = $ordered_fields;
    return $fields;

}


add_filter("woocommerce_checkout_fields", "order_shipping_fields");

function order_shipping_fields($fields) {

    $order = array(
        "shipping_first_name", 
        "shipping_last_name", 
        "shipping_address_1", 
        "shipping_address_2", 
        "shipping_postcode", 
        "shipping_city", 
        "shipping_country", 

    );
    foreach($order as $field)
    {
        $ordered_fields[$field] = $fields["shipping"][$field];
    }

    $fields["shipping"] = $ordered_fields;
    return $fields;

}

// Disable select2

// add_action( 'wp_enqueue_scripts', 'mgt_dequeue_stylesandscripts', 100 );

// function mgt_dequeue_stylesandscripts() {
//     if ( class_exists( 'woocommerce' ) ) {
//         wp_dequeue_style( 'select2' );
//         wp_deregister_style( 'select2' );

//         wp_dequeue_script( 'select2');
//         wp_deregister_script('select2');

//     } 
// } 

?>