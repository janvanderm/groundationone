<?php 

add_action('init', 'register_custom_taxonomy');
function register_custom_taxonomy() {
register_taxonomy( 'taxonomy_name',array (
  0 => 'custom_post_type',
),
array( 'hierarchical' => true,
  'label' => 'Taxonomy Name',
  'show_ui' => true,
  'query_var' => true,
  'show_admin_column' => false,
  'labels' => array (
  'search_items' => 'Taxonomy Name',
  'popular_items' => '',
  'all_items' => '',
  'parent_item' => '',
  'parent_item_colon' => '',
  'edit_item' => '',
  'update_item' => '',
  'add_new_item' => '',
  'new_item_name' => '',
  'separate_items_with_commas' => '',
  'add_or_remove_items' => '',
  'choose_from_most_used' => '',
)
) ); 
}

?>