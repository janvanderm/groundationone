<?php

// ADD SPACE AFTER EURO SIGN

add_action('admin_init', 'wpb_imagelink_setup', 10);
function jigoshop_add_my_currency_symbol( $currency_symbol, $currency ) {
    if ($currency=='EUR') $currency_symbol = '&euro; ';
    return $currency_symbol;
}
add_filter('jigoshop_currency_symbol', 'jigoshop_add_my_currency_symbol', 1, 2);

?>