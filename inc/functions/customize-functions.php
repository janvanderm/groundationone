<?php
function Ari_customize_register( $wp_customize ) {


// Add Sections

$wp_customize->add_section('layout' , array(
    'title' => __('Layout','Ari'),
));

$wp_customize->add_section('footer' , array(
    'title' => __('Footer','Ari'),
));


// Add Input

$wp_customize->add_setting('copyright_textbox', array());

$wp_customize->add_control(
    'copyright_textbox',
    array(
        'label' => 'Copyright text',
        'section' => 'footer',
        'type' => 'text',
    )
);

// Add Radio Button

$wp_customize->add_setting('sidebar_position', array());

$wp_customize->add_control('sidebar_position', array(
  'label'      => __('Sidebar position', 'Ari'),
  'section'    => 'layout',
  'settings'   => 'sidebar_position',
  'type'       => 'radio',
  'choices'    => array(
    'left'   => 'left',
    'right'  => 'right',
  ),
));


// Add Select

$wp_customize->add_setting(
    'powered_by',
    array(
        'default' => 'wordpress',
    )
);
 
$wp_customize->add_control(
    'powered_by',
    array(
        'type' => 'select',
        'label' => 'This site is powered by:',
        'section' => 'layout',
        'choices' => array(
            'wordpress' => 'WordPress',
            'hamsters' => 'Hamsters',
            'jet-fuel' => 'Jet Fuel',
            'nuclear-energy' => 'Nuclear Energy',
        ),
    )
);




}
add_action( 'customize_register', 'Ari_customize_register' );
?>