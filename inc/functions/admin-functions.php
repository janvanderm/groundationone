<?php

// disable dashboard widgets
function disable_default_dashboard_widgets() {

	// removing default dashboard widgets
	remove_meta_box( 'dashboard_right_now', 'dashboard', 'core' );    // Right Now Widget
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'core' ); // Comments Widget
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'core' );  // Incoming Links Widget
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'core' );         // Plugins Widget
	remove_meta_box( 'dashboard_activity', 'dashboard', 'core' );         // Activity
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'core' );   // Quick Press Widget
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'core' );   // Recent Drafts Widget
	remove_meta_box( 'dashboard_primary', 'dashboard', 'core' );         //
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'core' );       //

	// removing plugin dashboard boxes
	remove_meta_box( 'yoast_db_widget', 'dashboard', 'normal' );         // Yoast's SEO Plugin Widget

}

// RSS Dashboard Widget
function bones_rss_dashboard_widget() {
	if ( function_exists( 'fetch_feed' ) ) {?>
    -
	<?php }
}

// calling all custom dashboard widgets
function bones_custom_dashboard_widgets() {
	wp_add_dashboard_widget( 'bones_rss_dashboard_widget', __( 'Dashboard Instruction Widget', 'bonestheme' ), 'bones_rss_dashboard_widget' );
	/*
	Be sure to drop any other created Dashboard Widgets
	in this function and they will all load.
	*/
}


// removing the dashboard widgets
add_action( 'admin_menu', 'disable_default_dashboard_widgets' );
// adding any custom widgets
add_action( 'wp_dashboard_setup', 'bones_custom_dashboard_widgets' );


/************* CUSTOM LOGIN PAGE *****************/

// calling your own login css so you can style it

//Updated to proper 'enqueue' method
//http://codex.wordpress.org/Plugin_API/Action_Reference/login_enqueue_scripts
function bones_login_css() {
	wp_enqueue_style( 'bones_login_css', get_template_directory_uri() . '/css/login.css', false );
}

// changing the logo link from wordpress.org to your site
function bones_login_url() {  return home_url(); }

// changing the alt text on the logo to show your site name
function bones_login_title() { return get_option( 'blogname' ); }

// calling it only on the login page
add_action( 'login_enqueue_scripts', 'bones_login_css', 10 );
add_filter( 'login_headerurl', 'bones_login_url' );
add_filter( 'login_headertitle', 'bones_login_title' );

/************* CUSTOMIZE ADMIN *******************/

/*
I don't really recommend editing the admin too much
as things may get funky if WordPress updates. Here
are a few funtions which you can choose to use if
you like.
*/

// Custom Backend Footer
function bones_custom_admin_footer() {
	_e( '<span id="footer-thankyou"><a href="h#" target="_blank">Developer / Agency</a>.</span>', 'bonestheme' );
}
// adding it to the admin area
add_filter( 'admin_footer_text', 'bones_custom_admin_footer' );


/*-----------------------------------------------------------------------------------*/
/* Remove Unwanted Admin Menu Items */
/*-----------------------------------------------------------------------------------*/

function remove_admin_menu_items() {
    $remove_menu_items = array(__('Links'));
    global $menu;
    end ($menu);
    while (prev($menu)){
        $item = explode(' ',$menu[key($menu)][0]);
        if(in_array($item[0] != NULL?$item[0]:"" , $remove_menu_items)){
        unset($menu[key($menu)]);}
    }
}

add_action('admin_menu', 'remove_admin_menu_items');

// Customize Menu Order
function custom_menu_order($menu_ord) {
    if (!$menu_ord) return true;
    return array(
        'index.php', 
        'separator1',
        'edit.php?post_type=page',
        'edit.php',
        'edit.php?post_type=custom',
        'formidable',
        'upload.php', 
        'separator2',
        'acf-options-header',
        'jigoshop',    
        'options-general.php', 
        'users.php',
        'separator-last',
        'wpseo_dashboard',    
        'jigoshop-ogonecw',    
        'backwpup',            
        'edit.php?post_type=search-filter-widget',        
        'edit.php?post_type=acf',                
        'themes.php',
        'plugins.php',
        'tools.php', 
    );
}
add_filter('custom_menu_order', 'custom_menu_order'); 
add_filter('menu_order', 'custom_menu_order');

// Disable Core Update Messages

// function remove_core_updates(){
// global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
// }
// add_filter('pre_site_transient_update_core','remove_core_updates');
// add_filter('pre_site_transient_update_plugins','remove_core_updates');
// add_filter('pre_site_transient_update_themes','remove_core_updates');

// Remove editor from admin

function remove_editor_menu() {
  remove_action('admin_menu', '_add_themes_utility_last', 101);
}
add_action('_admin_menu', 'remove_editor_menu', 1);


// Disable default widgets

// -----------------------------------
// --  REMOVE DEFAULT WIDGETS  --
// -----------------------------------
 
function pc_unregister_default_widgets() {
    unregister_widget('WP_Widget_Pages');
    unregister_widget('WP_Widget_Calendar');
    unregister_widget('WP_Widget_Archives');
    unregister_widget('WP_Widget_Links');
    unregister_widget('WP_Widget_Categories');
    unregister_widget('WP_Widget_Meta');
    unregister_widget('WP_Widget_Recent_Comments');
    unregister_widget('WP_Widget_Recent_Posts');
    unregister_widget('WP_Widget_Search');
    unregister_widget('WP_Widget_Text');
    unregister_widget('WP_Nav_Menu_Widget');
    unregister_widget('WP_Widget_RSS');
    unregister_widget('WP_Widget_Tag_Cloud');
    unregister_widget('Twenty_Eleven_Ephemera_Widget');
}
 
add_action( 'widgets_init', 'pc_unregister_default_widgets', 11 );


// Remove admin columns

function my_columns_filter( $columns ) {
    unset($columns['author']);
    unset($columns['tags']);
    unset($columns['categories']);
    unset($columns['date']);
    unset($columns['tags']);
    unset($columns['comments']);
    unset($columns['wpseo-title']);
    unset($columns['wpseo-metadesc']);
    unset($columns['wpseo-focuskw']);
    unset($columns['wpseo-score']);
    return $columns;
}
add_filter( 'manage_edit-post_columns', 'my_columns_filter', 10, 1 );
add_filter( 'manage_edit-page_columns', 'my_columns_filter', 10, 1 );
add_filter( 'manage_edit-product_columns', 'my_columns_filter', 10, 1 );
add_filter( 'manage_edit-product_columns', 'my_columns_filter', 10, 1 );

// Remove menu items from top bar

function mytheme_admin_bar_render() {
global $wp_admin_bar;
    // MAIN
    $wp_admin_bar->remove_menu('wpseo-menu');
    $wp_admin_bar->remove_menu('w3tc');
    $wp_admin_bar->remove_menu('comments');
    $wp_admin_bar->remove_menu('backwpup');
    $wp_admin_bar->remove_menu('wp-logo');
    // SUB MENUS
    $wp_admin_bar->remove_node('new-post');
    $wp_admin_bar->remove_node('new-user');
    $wp_admin_bar->remove_node('new-shop_order');

}
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );

// Disable Admin Bar for everyone

if (!function_exists('df_disable_admin_bar')) {

    function df_disable_admin_bar() {
        
        // for the admin page
        remove_action('admin_footer', 'wp_admin_bar_render', 1000);
        // for the front-end
        remove_action('wp_footer', 'wp_admin_bar_render', 1000);
            
        // css override for the admin page
        function remove_admin_bar_style_backend() { 
            echo '<style>body.admin-bar #wpcontent, body.admin-bar #adminmenu { padding-top: 0px !important; }</style>';
        }   
        add_filter('admin_head','remove_admin_bar_style_backend');
        
        // css override for the frontend
        function remove_admin_bar_style_frontend() {

            if ( is_user_logged_in() ) {
                echo '<style type="text/css" media="screen">
                html { margin-top: 0px !important; }
                * html body { margin-top: 0px !important; }
                </style>';
            }
            
        }
        add_filter('wp_head','remove_admin_bar_style_frontend', 99);
        }
}
add_action('init','df_disable_admin_bar');


// Disable default image placement link
function wpb_imagelink_setup() {
    $image_set = get_option( 'image_default_link_type' );
    
    if ($image_set !== 'none') {
        update_option('image_default_link_type', 'none');
    }
}
add_action('admin_init', 'wpb_imagelink_setup', 10);

// Add Typekit admin styles
// add_filter("mce_external_plugins", "tomjn_mce_external_plugins");
// function tomjn_mce_external_plugins($plugin_array){
//     $plugin_array['typekit']  =  get_template_directory_uri().'/js/app-admin-tinymce.min.js';
//     return $plugin_array;
// }

// Keep tinymce from removing P tages
// remove_filter( 'the_content', 'wpautop' );
// remove_filter( 'the_excerpt', 'wpautop' );

?>